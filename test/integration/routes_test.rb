require 'test_helper'

class RoutesTest < ActionDispatch::IntegrationTest
  test "Verify root redirection" do
    assert_recognizes({controller: 'subscribers', action: 'new'}, {path: '', method: :get})
  end

  test "Subscriber routes" do
    assert_generates "/subscribers/new", {controller: "subscribers", action: "new"}
    assert_generates "/subscribers", {controller: "subscribers", action: "create"}

    assert_recognizes({controller: 'subscribers', action: 'create'}, {path: '/subscribers', method: :post})
  end
end