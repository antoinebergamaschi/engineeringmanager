require 'test_helper'

class SubscriberTest < ActiveSupport::TestCase
  setup do
    Subscriber.delete_all
  end

  test "should not save subscribers without login" do
    subscriber = Subscriber.new

    assert_raises(Subscriber::InvalidLoginFormatError, "Subscriber saved without login") do
      subscriber.save
    end
  end

  test "should not save subscribers with login that already exist" do
    login = "DEF"
    Subscriber.create(login: login)

    subscriber = Subscriber.new(login: login)

    assert_raises(Subscriber::LoginUniquenessError, "Subscriber saved with duplicated login") do
      subscriber.save
    end
  end

  test "should not save subscribers without a valid login format" do
    ["", "def", "d", "1", "TE1", 132].each do |login|
      subscriber = Subscriber.new(login: login)

      assert_raises(Subscriber::InvalidLoginFormatError, "Subscriber saved with invalid login format") do
        subscriber.save
      end
    end
  end


  test "should generate random valid login" do
    100.times do
      login = Subscriber.random_login
      subscriber = Subscriber.new(login: login)

      # LoginUniquenessError are skipped here ( if any )
      assert subscriber.save rescue Subscriber::LoginUniquenessError ensure next
    end
  end

  test "should always generate a new subscriber" do
    100.times do
      login = (/[A-Za-z0-9]{#{rand(5).to_i}}/).random_example
      assert_nothing_raised do
        subscriber = Subscriber.force_creation(login, :random_login)

        assert_not_nil subscriber.id
      end
    end
  end

  test "should always generate a new subscriber in descent amount of time" do
    generated_subscribers= (Subscriber::MAX_LOGIN_NUMBER*0.9).to_i
    generate_data(generated_subscribers)

    remaining_available_logins = Subscriber::MAX_LOGIN_NUMBER  - generated_subscribers
    raise "not enough login available" if 100 > remaining_available_logins

    sum = 0
    100.times do
      assert_nothing_raised do
        login = (/[A-Za-z0-9]{#{rand(5).to_i}}/).random_example
        sum += Benchmark.ms {
          Subscriber.force_creation(login)
        }
      end
    end

    # Giving 200ms by creation
    assert(200*100 > sum, "Takes to long !")
  end

  test "should raise an exception if no more available logins" do
    generate_data(Subscriber::MAX_LOGIN_NUMBER)

    assert_raises(Subscriber::NoMoreLoginAvailable, "No more login available") do
      Subscriber.force_creation(Subscriber::FIRST_LOGIN)
    end
  end

  private

  # Fill the DB with Subscriber records
  def generate_data(number)
    login = Subscriber::FIRST_LOGIN.dup
    data = [{login: login.dup, created_at: Time.now, updated_at: Time.now}]
    number.times{ data << {login: login.succ!.dup, created_at: Time.now, updated_at: Time.now} }

    Subscriber.insert_all!(data)
  end
end