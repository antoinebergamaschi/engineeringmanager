require 'test_helper'

class SubscribersControllerTest < ActionDispatch::IntegrationTest
  setup do
  end

  test "should get new" do
    get new_subscriber_url
    assert_response :success
  end

  test "should create subscribers" do
    assert_difference('Subscriber.count') do
      post subscribers_url, params: { subscriber: { login: "MOO" } }
    end

    assert_response :success
  end
end
