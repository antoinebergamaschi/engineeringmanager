class SubscribersController < ApplicationController

  def new
    @subscriber = Subscriber.new
  end

  def create
    subscriber_params = params.require(:subscriber).permit(:login)
    subscriber = Subscriber.force_creation(subscriber_params[:login])

    is_generated = (subscriber.login != subscriber_params[:login])

    render :json => {login: subscriber.login, generated_login: is_generated}
  end
end
