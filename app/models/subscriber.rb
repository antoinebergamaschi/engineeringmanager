class Subscriber < ApplicationRecord

  class InvalidLoginFormatError < StandardError
  end

  class LoginUniquenessError < StandardError
  end

  class NoMoreLoginAvailable < StandardError
  end

  # Regex Allows only :
  # - Alpha uppercase character between A-Z
  # - With exactly 3 characters
  #
  # Warning if +LOGIN_FORMAT_REGEX+ is modified the linked +FIRST_LOGIN/LAST_LOGIN/MAX_LOGIN_NUMBER+ must also be corrected
  LOGIN_FORMAT_REGEX = /^[A-Z]{3}$/
  FIRST_LOGIN = "AAA".freeze
  LAST_LOGIN = "ZZZ".freeze
  # -1 is for table index format
  MAX_LOGIN_NUMBER = (26.pow(3) - 1).freeze

  validate :verify_login_format
  validates :login, uniqueness: true, strict: LoginUniquenessError

  # @return [String] A random string matching the +LOGIN_FORMAT_REGEX+
  # @see RegexpExamples
  def Subscriber.random_login
    return LOGIN_FORMAT_REGEX.random_example
  end


  # @return [String] The first missing String from every logins sorted in ASC order
  def Subscriber.succ_login
    # Load Entire login DB values ( should be ASC ordered to match String#succ function )
    stored_logins = Subscriber.select(:login).order("login ASC").collect(&:login)

    # If no login exist create the first Login compliant with the +LOGIN_FORMAT_REGEX+ in ASC order
    if stored_logins.blank?
      return FIRST_LOGIN
    end

    test_login = FIRST_LOGIN.dup

    stored_logins.each do |login|
      # While next login in the sorted Array is equal to the next String computed in Alpha ASC ordering
      if login == test_login && test_login != LAST_LOGIN
        test_login.succ!
      else
        # A test_login != to any login in the DB has been found
        break
      end
    end

    return test_login
  end

  # Ensure a new Subscriber will be created ( if there exist at least one available login )
  # @param login [String]
  # @param login_generation_fn [Symbol] A reference to a login generation function that should be defined in Subscriber Class
  # @return [Subscriber] The new subscriber login
  def Subscriber.force_creation(login, login_generation_fn = :succ_login)
    subscriber = Subscriber.new(login: login)

    begin
      subscriber.save
    rescue Subscriber::InvalidLoginFormatError, Subscriber::LoginUniquenessError

      # Extreme case when no more login are available with the given +LOGIN_FORMAT_REGEX+
      if Subscriber.count > MAX_LOGIN_NUMBER
        raise Subscriber::NoMoreLoginAvailable
      end

      return Subscriber.force_creation(send(login_generation_fn), login_generation_fn)
    end

    return subscriber
  end

  private

  def verify_login_format
    # If the login does not match the LOGIN_FORMAT_REGEX
    if (self.login =~ LOGIN_FORMAT_REGEX) != 0
      self.errors.add(:login, "is expected to follow the format: #{LOGIN_FORMAT_REGEX.inspect}")
      raise InvalidLoginFormatError.new(self)
    end
  end
end
