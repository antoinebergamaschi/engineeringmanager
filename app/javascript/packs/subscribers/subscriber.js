// Add listeners on the subscribers form DOM to display Back-end responses
document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
        // init Listeners to Ajax Call
        let dom = document.getElementById("subscriberCreationForm");
        dom.addEventListener('ajax:success', function(event) {
            // Expects
            // data => {login: String, generated_login: Boolean}
            let data = event.detail[0];

            let resultContainer = document.getElementById("resultContainer");
            let newSubscriber = document.createElement("p");
            newSubscriber.innerHTML = data.login;

            if (data.generated_login){
                newSubscriber.style.color = "orange";
            }else{
                newSubscriber.style.color = "green";
            }

            resultContainer.append(newSubscriber);
        })

        dom.addEventListener('ajax:error', function(event) {
            if (window.console){
                console.log(event);
            }
        })
    }
}

