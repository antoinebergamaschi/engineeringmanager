class CreateSubscribers < ActiveRecord::Migration[6.0]
  def change
    create_table :subscribers do |t|
      t.string :login, null: false
      t.timestamps
    end
  end
end
